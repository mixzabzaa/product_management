// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require assets/global/plugins/bootstrap/js/bootstrap.min
//= require assets/global/plugins/js.cookie.min
//= require assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min
//= require assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min
//= require assets/global/plugins/jquery.blockui.min
//= require assets/global/plugins/uniform/jquery.uniform.min
//= require assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min

//= require moment/moment
//= require moment/min/moment-with-locales
//= require assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable
//= require assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput
//= require assets/global/scripts/datatable
//= require assets/global/plugins/datatables/datatables.min
//= require assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap
//= require assets/global/plugins/fullcalendar/fullcalendar.min
//= require assets/global/plugins/select2/js/select2.full.min
//= require bootstrap-toggle/js/bootstrap-toggle.min
//= require jquery-textcomplete/dist/jquery.textcomplete
//= require mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker

//= require assets/global/scripts/app

//= require assets/layouts/layout/scripts/layout.min

//= require general/asterisk
//= require general/bootstrap
//= require general/button_clicked
//= require general/calendar
//= require general/colorpicker
//= require general/datatable
//= require general/editable
//= require general/extend_jquery
//= require general/hyperlink_modal
//= require vue/dist/vue
//= require_self
