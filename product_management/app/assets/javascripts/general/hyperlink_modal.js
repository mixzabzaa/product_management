(function ($, undefined) {
  // Get position of current cursor in textaread
  $.fn.addLinkToCursorPosition = function (text) {
    var el = $(this).get(0);
    var pos = 0;
    if ('selectionStart' in el) {
      pos = el.selectionStart;
    } else if ('selection' in document) {
      el.focus();
      var Sel = document.selection.createRange();
      var SelLength = document.selection.createRange().text.length;
      Sel.moveStart('character', -el.value.length);
      pos = Sel.text.length - SelLength;
    }
    var content = $(this).val();
    var newContent = content.substr(0, pos) + text + content.substr(pos);
    $(this).val(newContent);
  }
})(jQuery);

// Make it to function to rebind event when add
function bind_hyperlink_modal(){
  var modal_html_array, modal_html, a_tag;
  $(".hyperlink-modal").next(".a-hyperlink-modal").remove();
  $(".hyperlink-modal").after("<a class=\"btn btn-primary pull-right a-hyperlink-modal\" style='margin-top:10px; margin-bottom: 10px;'>Add Link</a>");
  $(".a-hyperlink-modal").click(function(){
    $self = $(this);

    // Modal html to inject when click on .a-hyper-link-modal
    modal_html_array = ['<div class="modal fade" id="a-tag-modal" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">',
                        '<div class="modal-dialog">',
                          '<div class="modal-content">',
                            '<div class="modal-header">',
                              '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>',
                              '<h4 class="modal-title">Add Hyperlink</h4>',
                            '</div>',
                            '<div class="modal-body">',
                              '<div class="row">',
                                '<div class="col-md-8 col-xs-12">',
                                  '<div class="form-group">',
                                    '<label>URL</label>',
                                    '<input type="text" class="form-control" id="url-hyperlink-modal" placeholder="E.g. http://www.google.com">',
                                  '</div>',
                                '</div>',
                                '<div class="col-md-4 col-md-12">',
                                  '<div class="form-group">',
                                    '<label>Title</label>',
                                    '<input type="text" class="form-control" id="title-hyperlink-modal" placeholder="E.g. Link to google">',
                                  '</div>',
                                '</div>',
                              '</div>',
                            '</div>',
                            '<div class="modal-footer">',
                              '<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>',
                              '<button type="button" class="btn green" id="add-hyperlink-btn">Add link</button>',
                            '</div>',
                          '</div>',
                        '</div>',
                      '</div>'];

    modal_html = modal_html_array.join("");
    $('body').append(modal_html);
    // show modal
    $('#a-tag-modal').modal('show');

    $('#add-hyperlink-btn').click(function(){
      var link = $("#url-hyperlink-modal").val();
      link = link.replace(/\s/g, '');
      if(link.substring(0, 4) !== "http"){
        link = "http://" + link;
      }
      a_tag = "<a href='"+ link +"'>"+ $("#title-hyperlink-modal").val() +"</a>";

      // Append a tag to hypelink-modal class
      $prev_text_area = $self.prev();
      $prev_text_area.addLinkToCursorPosition(a_tag);

      // Update field value
      var div_id = $prev_text_area.closest('.pk-form-back-input').attr('id');
      var val = $.sanitize_html($prev_text_area.val());
      if (val.length <= 0) {
        $('.pk-row-back-field-cell#' + div_id).find('.pk-row-back-field-value').html('')
      } else {
        $('.pk-row-back-field-cell#' + div_id).find('.pk-row-back-field-value').html(val)
      }

      // Hide modal
      $('#a-tag-modal').modal('hide');
    })

    // remove modal on hidden
    $('#a-tag-modal').on('hidden.bs.modal', function (e) {
      $(this).remove();
    })
  });
}

bind_hyperlink_modal();
