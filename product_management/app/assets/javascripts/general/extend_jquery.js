(function($) {
  $.sanitize = function(input) {
    var output = input.replace(/<script[^>]*?>.*?<\/script>/gi, '').
      replace(/<(?!\/?a(?=>|\s.*>))\/?.*?>/gi, '').
      replace(/<style[^>]*?>.*?<\/style>/gi, '').
      replace(/<![\s\S]*?--[ \t\n\r]*>/gi, '');

    return output;
  };
  $.sanitize_html = function(input){
    var output = $.sanitize(input)
    output = $.add_target_blank(output);
    output = output.replace(/\n/g, "<br />");
    return output;
  }
  $.is_snake_case = function(input){
    return input.replace(' ','').length === input.length
  }
  $.add_target_blank = function(html) {
    var container = document.createElement("p");
    container.innerHTML = html;
    var anchors = container.getElementsByTagName("a");
    for (var i = 0; i < anchors.length; i++) {
      anchors[i].setAttribute('target', '_blank');
    }
    return container.innerHTML;
  }
})(jQuery);
