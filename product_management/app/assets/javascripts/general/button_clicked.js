$(document).ready(function(){
  $('a.btn:not(.always-on)').data('loadingText','<i class="fa fa-spinner fa-spin"></i>')
  $('button:submit:not(.always-on)').data('loadingText','<i class="fa fa-spinner fa-spin"></i>')
  var loadingTime = $('a.btn:not(.always-on)').data('loadingTime')
  $('a.btn:not(.always-on)').click(function() {
    var btn = $(this);
    btn.button('loading');
    if (loadingTime) {
      setTimeout(function () {
          btn.button('reset')
      }, loadingTime * 1000)
    } else {
      setTimeout(function () {
          btn.button('reset')
      }, 8000)
    }
  });
  $('button:submit').click(function() {
    var btn = $(this);
    btn.button('loading')
    btn.closest('form').submit();
    setTimeout(function () {
        btn.button('reset')
    }, 10000)
  });
})
