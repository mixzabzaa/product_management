var ColorPickerManaged = function() {
  var initColorpicker = function() {
    var el = $('.should-colorpicker');
    el.colorpicker()
  }
  return {
    init: function() {
      if (!jQuery().colorpicker) {
        return;
      }
      initColorpicker();
    }
  };
}();
ColorPickerManaged.init();
