var TableDatatablesManaged = function() {
  var initTable = function(table) {
    var table = $(table)
    if (table.hasClass('dataTable')) {
      return;
    }
    table.dataTable({
      // Internationalisation. For more info refer to http://datatables.net/manual/i18n
      "language": {
        "aria": {
          "sortAscending": ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ records",
        "infoEmpty": "No records found",
        "infoFiltered": "(filtered1 from _MAX_ total records)",
        "lengthMenu": "Show _MENU_",
        "search": "Search:",
        "zeroRecords": "No matching records found",
        "paginate": {
          "previous": "Prev",
          "next": "Next",
          "last": "Last",
          "first": "First"
        }
      },
      buttons: [
        { extend: 'excel', className: 'btn yellow btn-outline ' },
        { extend: 'csv', className: 'btn purple btn-outline ' }
      ],

      "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

      "lengthMenu": [
        [5, 15, 20, 30, -1],
        [5, 15, 20, 30, "All"] // change per page values here
      ],
      // set the initial value
      "pageLength": 30,
      "pagingType": "bootstrap_full_number"
    });

    var tool_buttons = '<div class="btn-group"><a class="btn blue btn-outline btn-circle always-on" href="javascript:;" data-toggle="dropdown"><span class="hidden-xs">&nbsp; Tools </span><i class="fa fa-angle-down"></i></a><ul class="dropdown-menu pull-right" id="sample_3_tools"><li><a href="javascript:;" data-action="0" class="tool-action"><i class="icon-paper-clip"></i> Excel </a></li><li><a href="javascript:;" data-action="1" class="tool-action"><i class="icon-cloud-upload"></i> CSV </a></li></ul></div>'
    var actions = table.closest('.portlet').find('.actions');
    actions.prepend(tool_buttons);
    actions.find('a.tool-action').on('click', function() {
        var action = $(this).attr('data-action');
        table.DataTable().button(action).trigger();
    });

    table.find('.group-checkable').change(function() {
      var set = jQuery(this).attr("data-set");
      var checked = jQuery(this).is(":checked");
      jQuery(set).each(function() {
        if (checked) {
          $(this).prop("checked", true);
          $(this).parents('tr').addClass("active");
        } else {
          $(this).prop("checked", false);
          $(this).parents('tr').removeClass("active");
        }
      });
      jQuery.uniform.update(set);
    });

    table.on('change', 'tbody tr .checkboxes', function() {
      $(this).parents('tr').toggleClass("active");
    });
  }

  return {
    init: function() {
      if (!jQuery().dataTable) {
        return;
      }
      var tables = $('.should-datatable');
      $.each(tables,function(i,table){
        initTable(table);
      })
    }
  };
}();
jQuery(document).ready(function() {
  TableDatatablesManaged.init();
});
