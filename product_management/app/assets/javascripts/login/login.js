//= require jquery
//= require jquery_ujs
//= require assets/global/plugins/bootstrap/js/bootstrap.min
//= require assets/global/plugins/js.cookie.min
//= require assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min
//= require assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min
//= require assets/global/plugins/jquery.blockui.min
//= require assets/global/plugins/uniform/jquery.uniform.min
//= require assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min

//= require assets/global/plugins/jquery-validation/js/jquery.validate.min
//= require assets/global/plugins/jquery-validation/js/additional-methods.min
//= require assets/global/plugins/select2/js/select2.full.min

//= require assets/global/scripts/app.min

//= require assets/pages/scripts/login.min

//= require ../general/button_clicked
//= require ../general/datatable
