# == Schema Information
#
# Table name: shops
#
#  id                 :integer          not null, primary key
#  uid                :text
#  name               :text
#  business_client_id :integer
#  address            :text
#  telephone          :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Shop < ApplicationRecord
  belongs_to :business_client

  has_many :employees
  has_many :orders
  has_many :shop_operation_times
  has_many :shop_products
  has_many :statuses
  has_many :time_interval_products

  validates :name, presence: true
  validates :address, presence: true
  validates :telephone, presence: true, format: { with: /\A[0-9()#-]*\z/ }
  validates :business_client, presence: true

end
