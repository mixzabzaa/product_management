# == Schema Information
#
# Table name: products
#
#  id                 :integer          not null, primary key
#  uid                :text
#  name               :text
#  price              :decimal(, )
#  quantity           :integer
#  business_client_id :integer
#  product_group_id   :integer
#  tag_criteria       :jsonb
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Product < ApplicationRecord
  belongs_to :business_client
  belongs_to :product_group

  has_many :product_add_ons
  has_many :shop_products

  validates :name , presence: true
  validates :business_client , presence: true

end
