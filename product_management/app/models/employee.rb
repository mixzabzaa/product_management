# == Schema Information
#
# Table name: employees
#
#  id                     :integer          not null, primary key
#  uid                    :text
#  title                  :text
#  first_name             :text
#  last_name              :text
#  telephone              :text
#  shop_id                :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  address                :text
#

class Employee < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  belongs_to :shop

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :title, presence: true
  validates :telephone, presence: true
  validates :email , presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }
  validates :shop_id, presence: true

  TITLES = [
    {th: "นาย", en: "Mr", value: "Mr"},
    {th: "นาง", en: "Mrs", value: "Mrs"},
    {th: "นางสาว", en: "Miss", value: "Miss"}
  ].freeze

  def full_name
    full_name = []
    full_name << self.title if !self.title.nil? && !self.title.blank?
    full_name << self.first_name if !self.first_name.nil? && !self.first_name.blank?
    full_name << self.last_name if !self.last_name.nil? && !self.last_name.blank?
    return "No Name" if full_name.size == 0
    full_name.join(" ")
  end

end
