# == Schema Information
#
# Table name: business_clients
#
#  id         :integer          not null, primary key
#  uid        :text
#  name       :text
#  key        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BusinessClient < ApplicationRecord

  has_many :admins
  has_many :products
  has_many :shops
  has_many :customers
  has_many :add_ons
  has_many :product_groups

  validates :key, presence: true
  validates :name, presence: true

end
