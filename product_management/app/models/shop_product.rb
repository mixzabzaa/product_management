# == Schema Information
#
# Table name: shop_products
#
#  id         :integer          not null, primary key
#  uid        :text
#  price      :decimal(, )
#  quantity   :integer
#  shop_id    :integer
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ShopProduct < ApplicationRecord
  belongs_to :shop
  belongs_to :product

  has_many :order_products
  has_many :shop_product_add_ons
  has_many :time_interval_products

  validates :product, presence: true
  validates :shop, uniqueness: { scope: :product_id }

  def get_product_price(product_id)
    product = @main_business_client.products.find(product_id)
    return product.price
  end

end
