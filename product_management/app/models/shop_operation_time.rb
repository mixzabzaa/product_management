# == Schema Information
#
# Table name: shop_operation_times
#
#  id         :integer          not null, primary key
#  uid        :text
#  shop_id    :integer
#  name       :text
#  start_time :datetime
#  end_time   :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ShopOperationTime < ApplicationRecord
  belongs_to :shop

  validates :name, presence: true
  validates :shop, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
  validate :start_time_must_before_end_time

  def start_time_must_before_end_time
    if self.start_time.to_i > self.end_time.to_i
      self.errors.add(:start_time, "must before end time")
    end
  end

end
