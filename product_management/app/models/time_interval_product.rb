# == Schema Information
#
# Table name: time_interval_products
#
#  id              :integer          not null, primary key
#  uid             :text
#  name            :text
#  start_time      :datetime
#  end_time        :datetime
#  shop_id         :integer
#  shop_product_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class TimeIntervalProduct < ApplicationRecord
  belongs_to :shop
  belongs_to :shop_product

  validates :shop_product, presence: true
  validates :shop, presence: true
  validate :start_time_must_before_end_time

  def start_time_must_before_end_time
    if self.start_time.to_i > self.end_time.to_i
      self.errors.add(:start_time, "must before end time")
    end
  end

end
