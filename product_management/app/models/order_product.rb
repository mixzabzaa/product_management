# == Schema Information
#
# Table name: order_products
#
#  id              :integer          not null, primary key
#  uid             :text
#  name            :text
#  price           :decimal(, )
#  quantity        :integer
#  order_id        :integer
#  shop_product_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OrderProduct < ApplicationRecord
  belongs_to :order
  belongs_to :shop_product

  has_many :order_product_add_ons
end
