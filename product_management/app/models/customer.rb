# == Schema Information
#
# Table name: customers
#
#  id                 :integer          not null, primary key
#  uid                :text
#  title              :text
#  first_name         :text
#  last_name          :text
#  email              :text
#  telephone          :text
#  address            :text
#  business_client_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Customer < ApplicationRecord
  belongs_to :business_client

  has_many :orders

  validates :title, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }, allow_blank: true
  validates :telephone, presence: true, format: { with: /\A[0-9()#-]*\z/ }
  validates :address, presence: true


  TITLES = [
    {th: "นาย", en: "Mr", value: "Mr"},
    {th: "นาง", en: "Mrs", value: "Mrs"},
    {th: "นางสาว", en: "Miss", value: "Miss"}
  ].freeze

  def full_name
    full_name = []
    full_name << self.title if !self.title.nil? && !self.title.blank?
    full_name << self.first_name if !self.first_name.nil? && !self.first_name.blank?
    full_name << self.last_name if !self.last_name.nil? && !self.last_name.blank?
    return "No Name" if full_name.size == 0
    full_name.join(" ")
  end
end
