# == Schema Information
#
# Table name: admins
#
#  id                     :integer          not null, primary key
#  uid                    :text
#  first_name             :text
#  last_name              :text
#  business_client_id     :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#

class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :business_client

  validates :email , presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }, allow_blank: true
  validates :business_client, presence: true

  def default_time_zone_preference
    self.time_zone_preference ||= "Bangkok"
  end

  def self.find_or_create_by_email(email, first_name = "", last_name = "", business_client)
    tmp_password = String.random(8)
    tmp_password = "1234567890" if Rails.env.development?
    admin = Admin.find_by_email(email) rescue nil
    admin ||= Admin.new(first_name: first_name, last_name: last_name, email: email, password: tmp_password, password_confirmation: tmp_password, business_client: business_client)
    admin.save
    admin
  end

  def full_name
    full_name = []
    full_name << self.first_name if !self.first_name.nil? && !self.first_name.blank?
    full_name << self.last_name if !self.last_name.nil? && !self.last_name.blank?
    return "No Name" if full_name.size == 0
    full_name.join(" ")
  end

end
