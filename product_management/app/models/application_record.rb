class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  before_validation :generate_uid, on: :create
  validates :uid, presence: true, uniqueness: true

  def generate_uid
    self.uid = String.random(10) if self.uid.blank?
  end

  def to_param
    uid
  end

end
