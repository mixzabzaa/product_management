# == Schema Information
#
# Table name: add_ons
#
#  id                 :integer          not null, primary key
#  uid                :text
#  name               :text
#  price              :integer
#  business_client_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class AddOn < ApplicationRecord
  belongs_to :business_client

  has_many :product_add_ons

  validates :name, presence: true
  validates :business_client, presence: true

end
