# == Schema Information
#
# Table name: statuses
#
#  id           :integer          not null, primary key
#  uid          :text
#  shop_id      :integer
#  name         :text
#  notification :text
#  step         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Status < ApplicationRecord
  belongs_to :shop

  has_many :orders

  validates :name, presence: true
  validates :shop, presence: true
end
