# == Schema Information
#
# Table name: order_product_add_ons
#
#  id                     :integer          not null, primary key
#  uid                    :text
#  shop_product_add_on_id :integer
#  order_product_id       :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class OrderProductAddOn < ApplicationRecord
  belongs_to :shop_product_add_on
  belongs_to :order_product
end
