# == Schema Information
#
# Table name: product_groups
#
#  id                 :integer          not null, primary key
#  uid                :text
#  business_client_id :integer
#  name               :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class ProductGroup < ApplicationRecord
  belongs_to :business_client

  has_many :products

  validates :name, presence: true
  validates :business_client, presence: true
  
end
