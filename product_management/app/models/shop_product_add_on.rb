# == Schema Information
#
# Table name: shop_product_add_ons
#
#  id                :integer          not null, primary key
#  uid               :text
#  name              :text
#  price             :decimal(, )
#  product_add_on_id :integer
#  shop_product_id   :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class ShopProductAddOn < ApplicationRecord
  belongs_to :product_add_on
  belongs_to :shop_product

  has_many :order_porduct_add_ons

  validates :name, presence: true
  validates :product_add_ons, presence: true
  validates :shop_product, uniqueness: { scope: :product_add_on_id }

end
