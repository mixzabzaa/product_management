# == Schema Information
#
# Table name: product_add_ons
#
#  id         :integer          not null, primary key
#  uid        :text
#  price      :decimal(, )
#  product_id :integer
#  add_on_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductAddOn < ApplicationRecord
  belongs_to :product
  belongs_to :add_on

  has_many :shop_product_add_ons

  validates :product, uniqueness: { scope: :add_on_id }
  validates :add_on, presence: true

end
