# == Schema Information
#
# Table name: orders
#
#  id          :integer          not null, primary key
#  uid         :text
#  total_price :decimal(, )
#  shop_id     :integer
#  customer_id :integer
#  status_id   :integer
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Order < ApplicationRecord
  belongs_to :shop
  belongs_to :customer
  belongs_to :status

  has_many :order_products

end
