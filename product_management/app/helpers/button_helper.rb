module ButtonHelper
  def button(path,color,icon_klass,delete=false)
    return raw("<a class='btn btn-icon-only ax-action-btn #{color}' href='#{path}'><i class='#{icon_klass}'></i></a>") if !delete
    raw("<a class='btn btn-icon-only ax-action-btn ax-trash-icon #{color}' href='#{path}' data-method='delete' data-confirm='are you sure'><i class='#{icon_klass}'></i></a>")
  end

  def edit_button(path)
    button(path,"yellow-lemon","icon-note")
  end

  def show_button(path)
    button(path,"green-sharp","fa fa-file-text-o")
  end

  def delete_button(path)
    button(path,"red-mint","icon-trash", true)
  end

  def activate_button(path)
    return raw("<a class='btn btn-icon-only ax-action-btn green-sharp' href='#{path}' data-method='post' data-confirm='are you sure'><i class='fa fa-check'></i></a>")
  end

  def inactivate_button(path)
    return raw("<a class='btn btn-icon-only ax-action-btn red-mint' href='#{path}' data-method='post' data-confirm='are you sure'><i class='fa fa-close'></i></a>")
  end
end
