json.extract! shop_operation_time, :id, :uid, :shop_id, :name, :start_time, :end_time, :created_at, :updated_at
json.url shop_operation_time_url(shop_operation_time, format: :json)