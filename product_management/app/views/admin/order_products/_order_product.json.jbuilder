json.extract! order_product, :id, :uid, :name, :price, :quantity, :order_id, :shop_product_id, :created_at, :updated_at
json.url order_product_url(order_product, format: :json)