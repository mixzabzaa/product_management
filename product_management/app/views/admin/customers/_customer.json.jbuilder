json.extract! customer, :id, :uid, :title, :first_name, :last_name, :email, :telephone, :address, :business_client_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)