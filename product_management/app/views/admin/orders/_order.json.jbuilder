json.extract! order, :id, :uid, :total_price, :shop_id, :customer_id, :status_id, :description, :created_at, :updated_at
json.url order_url(order, format: :json)