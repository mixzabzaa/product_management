json.extract! shop_product, :id, :uid, :price, :quantity, :shop_id, :product_id, :created_at, :updated_at
json.url shop_product_url(shop_product, format: :json)