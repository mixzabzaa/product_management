json.extract! employee, :id, :uid, :title, :first_name, :last_name, :telephone, :shop_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)