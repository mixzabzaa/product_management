json.extract! business_client, :id, :uid, :name, :key, :created_at, :updated_at
json.url business_client_url(business_client, format: :json)