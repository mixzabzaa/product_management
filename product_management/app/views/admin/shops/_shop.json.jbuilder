json.extract! shop, :id, :uid, :name, :business_client_id, :address, :telephone, :created_at, :updated_at
json.url shop_url(shop, format: :json)