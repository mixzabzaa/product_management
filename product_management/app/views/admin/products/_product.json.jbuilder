json.extract! product, :id, :uid, :name, :price, :quantity, :business_client_id, :product_group_id, :tag_criteria, :created_at, :updated_at
json.url product_url(product, format: :json)