json.extract! status, :id, :uid, :shop_id, :name, :notification, :step, :created_at, :updated_at
json.url status_url(status, format: :json)