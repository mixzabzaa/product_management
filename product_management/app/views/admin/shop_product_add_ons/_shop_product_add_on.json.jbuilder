json.extract! shop_product_add_on, :id, :uid, :name, :price, :product_add_on_id, :shop_product_id, :created_at, :updated_at
json.url shop_product_add_on_url(shop_product_add_on, format: :json)