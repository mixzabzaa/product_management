json.extract! product_add_on, :id, :uid, :price, :product_id, :add_on_id, :created_at, :updated_at
json.url product_add_on_url(product_add_on, format: :json)