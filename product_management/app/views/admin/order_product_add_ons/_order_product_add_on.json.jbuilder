json.extract! order_product_add_on, :id, :uid, :shop_product_add_on_id, :order_product_id, :created_at, :updated_at
json.url order_product_add_on_url(order_product_add_on, format: :json)