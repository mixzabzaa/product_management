json.extract! admin, :id, :uid, :first_name, :last_name, :business_client_id, :created_at, :updated_at
json.url admin_url(admin, format: :json)