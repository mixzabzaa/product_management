json.extract! add_on, :id, :uid, :name, :price, :business_client_id, :created_at, :updated_at
json.url add_on_url(add_on, format: :json)