class Admin::ShopOperationTimesController < Admin::AppController
  before_action :set_shop
  before_action :set_shop_operation_time, only: [:show, :edit, :update, :destroy]

  # GET /shop_operation_times
  # GET /shop_operation_times.json
  def index
    @shop_operation_times = @shop.shop_operation_times
  end

  # GET /shop_operation_times/1
  # GET /shop_operation_times/1.json
  def show
  end

  # GET /shop_operation_times/new
  def new
    @shop_operation_time = ShopOperationTime.new
  end

  # GET /shop_operation_times/1/edit
  def edit
  end

  # POST /shop_operation_times
  # POST /shop_operation_times.json
  def create
    @shop_operation_time = ShopOperationTime.new(shop_operation_time_params)
    @shop_operation_time.shop = @shop

    respond_to do |format|
      if @shop_operation_time.save
        format.html { redirect_to admin_shop_shop_operation_times_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: 'Shop operation time was successfully created.' }
        format.json { render :show, status: :created, location: @shop_operation_time }
      else
        format.html { render :new }
        format.json { render json: @shop_operation_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shop_operation_times/1
  # PATCH/PUT /shop_operation_times/1.json
  def update
    respond_to do |format|
      if @shop_operation_time.update(shop_operation_time_params)
        format.html { redirect_to admin_shop_shop_operation_time_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid: @shop_operation_time.uid), notice: 'Shop operation time was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop_operation_time }
      else
        format.html { render :edit }
        format.json { render json: @shop_operation_time.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shop_operation_times/1
  # DELETE /shop_operation_times/1.json
  def destroy
    @shop_operation_time.destroy
    respond_to do |format|
      format.html { redirect_to shop_operation_times_url, notice: 'Shop operation time was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = @main_business_client.shops.find_by_uid(params[:shop_uid])
      raise "NotFoud" if @shop.nil?
    end

    def set_shop_operation_time
      @shop_operation_time = @shop.shop_operation_times.find_by_uid(params[:uid])
      raise "NotFoud" if @shop_operation_time.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shop_operation_time_params
      params.require(:shop_operation_time).permit(:name, :start_time, :end_time)
    end
end
