class Admin::AppController < ApplicationController
  layout 'admin/application'

  protect_from_forgery with: :exception

  before_action :authenticate_admin!
  before_action :set_admin
  before_action :set_main_business_client
  before_action :set_locale

  def set_admin
    @admin = current_admin
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    @current_locale = I18n.locale.to_s
    @current_locale = "us" if I18n.locale == :en
  end

  def set_main_business_client
    @main_business_client = current_admin.business_client rescue nil
    if @main_business_client.nil? && current_admin.business_client.count > 0
      @main_business_client = current_admin.business_clients.first
      redirect_to admin_root_path(business_client_key: @main_business_client.key)
    end
  end

  def default_url_options
    { locale: (I18n.locale || I18n.default_locale) }
  end

end
