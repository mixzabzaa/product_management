class Admin::ShopProductsController < Admin::AppController
  before_action :set_shop
  before_action :set_shop_product, only: [:show, :create, :edit, :update, :destroy]

  # GET /shop_products
  # GET /shop_products.json
  def index
    @shop_products = @shop.shop_products
  end

  # GET /shop_products/1
  # GET /shop_products/1.json
  def show
  end

  # GET /shop_products/new
  def new
    @products = @main_business_client.products.where.not(id: @shop.shop_products.map(&:product).flatten.map(&:id))
  end

  # GET /shop_products/1/edit
  def edit
  end

  # POST /shop_products
  # POST /shop_products.json
  def create
    has_error = false
    error = ""
    if !params[:product_ids].nil?
      begin
        ActiveRecord::Base.transaction do
          params[:product_ids].each do |product_id|
            product_id = product_id.to_i
            product = @main_business_client.products.find(product_id)
            price = product.price
            ShopProduct.create!(shop_id: @shop.id, product_id: product_id, price: price)
          end
        end
      rescue ActiveRecord::RecordInvalid => e
        has_error = true
        error = e
      end
      if has_error
        flash[:alert] = "#{error}"
        render :new
      else
        redirect_to admin_shop_shop_products_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: t("notices.messages.shop_product.successfully_created")
      end
    else
      redirect_to new_admin_shop_shop_product_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), alert: t("notices.messages.shop_product.can_not_create_shop_product")
    end

    # respond_to do |format|
    #   if @shop_product.save
    #     format.html { redirect_to admin_shop_shop_products_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: 'Shop product was successfully created.' }
    #     format.json { render :show, status: :created, location: @shop_product }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @shop_product.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /shop_products/1
  # PATCH/PUT /shop_products/1.json
  def update
    respond_to do |format|
      if @shop_product.update(shop_product_params)
        format.html { redirect_to admin_shop_shop_products_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid:@shop_product.uid), notice: 'Shop product was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop_product }
      else
        format.html { render :edit }
        format.json { render json: @shop_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shop_products/1
  # DELETE /shop_products/1.json
  def destroy
    @shop_product.destroy
    respond_to do |format|
      format.html { redirect_to admin_shop_shop_products_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: 'Shop product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = @main_business_client.shops.find_by_uid(params[:shop_uid])
      raise "NotFoud" if @shop.nil?
    end

    def set_shop_product
      @shop_product = @shop.shop_products.find_by_uid(params[:uid])
      raise "NotFoud" if @shop.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shop_product_params
      p = params.require(:shop_product).permit(:price, :quantity)
      p[:shop_id] = @shop.id
      p
    end
end
