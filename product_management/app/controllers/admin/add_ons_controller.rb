class Admin::AddOnsController < Admin::AppController
  before_action :set_add_on, only: [:show, :edit, :update, :destroy]

  # GET /add_ons
  # GET /add_ons.json
  def index
    @add_ons = @main_business_client.add_ons
  end

  # GET /add_ons/1
  # GET /add_ons/1.json
  def show
  end

  # GET /add_ons/new
  def new
    @add_on = AddOn.new
  end

  # GET /add_ons/1/edit
  def edit
  end

  # POST /add_ons
  # POST /add_ons.json
  def create
    @add_on = AddOn.new(add_on_params)
    @add_on.business_client = @main_business_client

    respond_to do |format|
      if @add_on.save
        format.html { redirect_to admin_add_ons_path(business_client_key: @main_business_client.key), notice: 'Add on was successfully created.' }
        format.json { render :show, status: :created, location: @add_on }
      else
        format.html { render :new }
        format.json { render json: @add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /add_ons/1
  # PATCH/PUT /add_ons/1.json
  def update
    respond_to do |format|
      if @add_on.update(add_on_params)
        format.html { redirect_to admin_add_on_path(business_client_key:@main_business_client.key, uid: @add_on.uid), notice: 'Add on was successfully updated.' }
        format.json { render :show, status: :ok, location: @add_on }
      else
        format.html { render :edit }
        format.json { render json: @add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /add_ons/1
  # DELETE /add_ons/1.json
  def destroy
    @add_on.destroy
    respond_to do |format|
      format.html { redirect_to add_ons_url, notice: 'Add on was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_add_on
      @add_on = @main_business_client.add_ons.find_by_uid(params[:uid])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def add_on_params
      params.require(:add_on).permit(:name, :price)
    end
end
