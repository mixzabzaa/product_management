class Admin::ProductAddOnsController < Admin::AppController
  before_action :set_product
  before_action :set_product_add_on, only: [:show, :edit, :update, :destroy]

  # GET /product_add_ons
  # GET /product_add_ons.json
  def index
    @product_add_ons = @product.product_add_ons
  end

  # GET /product_add_ons/1
  # GET /product_add_ons/1.json
  def show
  end

  # GET /product_add_ons/new
  def new
    @add_ons = @main_business_client.add_ons.where.not(id: @product.product_add_ons.map(&:add_on).flatten.map(&:id))
  end

  # GET /product_add_ons/1/edit
  def edit
  end

  # POST /product_add_ons
  # POST /product_add_ons.json
  def create
    has_error = false
    error = ""
    if !params[:add_on_ids].nil?
      begin
        ActiveRecord::Base.transaction do
          params[:add_on_ids].each do |add_on_id|
            add_on_id = add_on_id.to_i
            add_on = @main_business_client.add_ons.find(add_on_id)
            price = add_on.price
            ProductAddOn.create!(product_id: @product.id, add_on_id: add_on_id, price: price)
          end
        end
      rescue ActiveRecord::RecordInvalid => e
        has_error = true
        error = e
      end
      if has_error
        flash[:alert] = "#{error}"
        render :new
      else
        redirect_to admin_product_group_product_path(business_client_key: @main_business_client.key, product_group_uid: @product.product_group.uid, uid: @product.uid), notice: t("notices.messages.product_add_on.successfully_created")
      end
    else
      redirect_to new_admin_product_group_product_product_add_on_path(business_client_key: @main_business_client.key, product_group_uid: @product.product_group.uid, product_uid: @product.uid), notice: t("notices.messages.product_add_on.cannot_created")
    end


    # respond_to do |format|
    #   if @product_add_on.save
    #     format.html { redirect_to @product_add_on, notice: 'Product add on was successfully created.' }
    #     format.json { render :show, status: :created, location: @product_add_on }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @product_add_on.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /product_add_ons/1
  # PATCH/PUT /product_add_ons/1.json
  def update
    respond_to do |format|
      if @product_add_on.update(product_add_on_params)
        format.html { redirect_to admin_product_group_product_path(business_client_key: @main_business_client.key, product_group_uid: @product.product_group.uid, uid: @product.uid), notice: 'Product add on was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_add_on }
      else
        format.html { render :edit }
        format.json { render json: @product_add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_add_ons/1
  # DELETE /product_add_ons/1.json
  def destroy
    @product_add_on.destroy
    respond_to do |format|
      format.html { redirect_to admin_product_group_product_path(business_client_key: @main_business_client.key, product_group_uid: @product.product_group.uid, uid: @product.uid), notice: 'Product add on was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_product
      @product = @main_business_client.products.find_by_uid(params[:product_uid])
      raise "NotFoud" if @product.nil?
    end
    def set_product_add_on
      @product_add_on = @product.product_add_ons.find_by_uid(params[:uid])
      raise "NotFound" if @product_add_on.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_add_on_params
      params.require(:product_add_on).permit(:price)
    end
end
