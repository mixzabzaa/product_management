class Admin::ShopProductAddOnsController < Admin::AppController
  before_action :set_shop
  before_action :set_shop_product
  before_action :set_shop_product_add_on, only: [:show, :edit, :update, :destroy]

  # GET /shop_product_add_ons
  # GET /shop_product_add_ons.json
  def index
    @shop_product_add_ons = ShopProductAddOn.all
  end

  # GET /shop_product_add_ons/1
  # GET /shop_product_add_ons/1.json
  def show
  end

  # GET /shop_product_add_ons/new
  def new
    @product_add_ons = @shop_product.product.product_add_ons.where.not(id: @shop_product.shop_product_add_ons.map(&:product_add_on).flatten.map(&:id))
  end

  # GET /shop_product_add_ons/1/edit
  def edit
  end

  # POST /shop_product_add_ons
  # POST /shop_product_add_ons.json
  def create
    has_error = false
    error = ""
    if !params[:product_add_on_ids].nil?
      begin
        ActiveRecord::Base.transaction do
          params[:product_add_on_ids].each do |product_add_on_id|
            product_add_on = @shop_product.product.product_add_ons.find(product_add_on_id)
            ShopProductAddOn.create!(shop_product_id: @shop_product.id, product_add_on_id: product_add_on.id, price: product_add_on.price, name: product_add_on.add_on.name)
          end
        end
      rescue ActiveRecord::RecordInvalid => e
        has_error = true
        error = e
      end
      if has_error
        flash[:alert] = "#{error}"
        render :new
      else
        redirect_to admin_shop_shop_product_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid: @shop_product.uid), notice: t("notices.messages.product_add_on.successfully_created")
      end
    else
      redirect_to new_admin_shop_shop_product_shop_product_add_on_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, shop_product_uid: @shop_product.uid), notice: t("notices.messages.product_add_on.cannot_created")
    end

    # respond_to do |format|
    #   if @shop_product_add_on.save
    #     format.html { redirect_to @shop_product_add_on, notice: 'Shop product add on was successfully created.' }
    #     format.json { render :show, status: :created, location: @shop_product_add_on }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @shop_product_add_on.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /shop_product_add_ons/1
  # PATCH/PUT /shop_product_add_ons/1.json
  def update
    respond_to do |format|
      if @shop_product_add_on.update(shop_product_add_on_params)
        format.html { redirect_to @shop_product_add_on, notice: 'Shop product add on was successfully updated.' }
        format.json { render :show, status: :ok, location: @shop_product_add_on }
      else
        format.html { render :edit }
        format.json { render json: @shop_product_add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shop_product_add_ons/1
  # DELETE /shop_product_add_ons/1.json
  def destroy
    @shop_product_add_on.destroy
    respond_to do |format|
      format.html { redirect_to admin_shop_shop_product_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid: @shop_product.uid), notice: 'Shop product add on was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = @main_business_client.shops.find_by_uid(params[:shop_uid])
      raise "Not Found" if @shop.nil?
    end

    def set_shop_product
      @shop_product = @shop.shop_products.find_by_uid(params[:shop_product_uid])
      raise "Not Found" if @shop.nil?
    end

    def set_shop_product_add_on
      @shop_product_add_on = @shop_product.shop_product_add_ons.find_by_uid(params[:uid])
      raise "Not Found" if @shop.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shop_product_add_on_params
      params.require(:shop_product_add_on).permit(:name, :price)
    end
end
