class Admin::OrderProductAddOnsController < Admin::AppController
  before_action :set_order_product_add_on, only: [:show, :edit, :update, :destroy]

  # GET /order_product_add_ons
  # GET /order_product_add_ons.json
  def index
    @order_product_add_ons = OrderProductAddOn.all
  end

  # GET /order_product_add_ons/1
  # GET /order_product_add_ons/1.json
  def show
  end

  # GET /order_product_add_ons/new
  def new
    @order_product_add_on = OrderProductAddOn.new
  end

  # GET /order_product_add_ons/1/edit
  def edit
  end

  # POST /order_product_add_ons
  # POST /order_product_add_ons.json
  def create
    @order_product_add_on = OrderProductAddOn.new(order_product_add_on_params)

    respond_to do |format|
      if @order_product_add_on.save
        format.html { redirect_to @order_product_add_on, notice: 'Order product add on was successfully created.' }
        format.json { render :show, status: :created, location: @order_product_add_on }
      else
        format.html { render :new }
        format.json { render json: @order_product_add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_product_add_ons/1
  # PATCH/PUT /order_product_add_ons/1.json
  def update
    respond_to do |format|
      if @order_product_add_on.update(order_product_add_on_params)
        format.html { redirect_to @order_product_add_on, notice: 'Order product add on was successfully updated.' }
        format.json { render :show, status: :ok, location: @order_product_add_on }
      else
        format.html { render :edit }
        format.json { render json: @order_product_add_on.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_product_add_ons/1
  # DELETE /order_product_add_ons/1.json
  def destroy
    @order_product_add_on.destroy
    respond_to do |format|
      format.html { redirect_to order_product_add_ons_url, notice: 'Order product add on was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_product_add_on
      @order_product_add_on = OrderProductAddOn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_product_add_on_params
      params.require(:order_product_add_on).permit(:uid, :shop_product_add_on_id, :order_product_id)
    end
end
