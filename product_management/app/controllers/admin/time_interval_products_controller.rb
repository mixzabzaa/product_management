class Admin::TimeIntervalProductsController < Admin::AppController
  before_action :set_shop
  before_action :set_time_interval_product, only: [:show, :edit, :update, :destroy]

  # GET /time_interval_products
  # GET /time_interval_products.json
  def index
    @time_interval_products = @shop.time_interval_products
  end

  # GET /time_interval_products/1
  # GET /time_interval_products/1.json
  def show
  end

  # GET /time_interval_products/new
  def new
    @shop_products = @shop.shop_products
    @time_interval_product = TimeIntervalProduct.new
  end

  # GET /time_interval_products/1/edit
  def edit
  end

  # POST /time_interval_products
  # POST /time_interval_products.json
  def create
    @time_interval_product = TimeIntervalProduct.new(time_interval_product_params)
    @time_interval_product.shop = @shop
    shop_product = ShopProduct.find(@time_interval_product.shop_product_id)
    @time_interval_product.name = shop_product.product.name

    respond_to do |format|
      if @time_interval_product.save
        format.html { redirect_to admin_shop_time_interval_products_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: 'Time interval product was successfully created.' }
        format.json { render :show, status: :created, location: @time_interval_product }
      else
        format.html { render :new }
        format.json { render json: @time_interval_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_interval_products/1
  # PATCH/PUT /time_interval_products/1.json
  def update
    respond_to do |format|
      if @time_interval_product.update(time_interval_product_params)
        format.html { redirect_to admin_shop_time_interval_product_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid: @time_interval_product.uid), notice: 'Time interval product was successfully updated.' }
        format.json { render :show, status: :ok, location: @time_interval_product }
      else
        format.html { render :edit }
        format.json { render json: @time_interval_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_interval_products/1
  # DELETE /time_interval_products/1.json
  def destroy
    @time_interval_product.destroy
    respond_to do |format|
      format.html { redirect_to time_interval_products_url, notice: 'Time interval product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = @main_business_client.shops.find_by_uid(params[:shop_uid])
      raise "NotFoud" if @shop.nil?
    end

    def set_time_interval_product
      @time_interval_product = @shop.time_interval_products.find_by_uid(params[:uid])
      raise "NotFoud" if @time_interval_product.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def time_interval_product_params
      params.require(:time_interval_product).permit(:start_time, :end_time, :shop_product_id, :shop_id)
    end
end
