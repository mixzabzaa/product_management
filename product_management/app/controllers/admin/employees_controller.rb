class Admin::EmployeesController < Admin::AppController
  before_action :set_shop
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  # GET /employees
  # GET /employees.json
  def index
    @employees = @shop.employees
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
  end

  # GET /employees/new
  def new
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    @employee.shop = @shop

    respond_to do |format|
      if @employee.save
        format.html { redirect_to admin_shop_employees_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid), notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to admin_shop_employee_path(business_client_key: @main_business_client.key, shop_uid: @shop.uid, uid: @employee.uid), notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shop
      @shop = @main_business_client.shops.find_by_uid(params[:shop_uid])
      raise "NotFoud" if @shop.nil?
    end

    def set_employee
      @employee = @shop.employees.find_by_uid(params[:uid])
      raise "NotFoud" if @employee.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:title, :first_name, :last_name, :telephone, :email, :address)
    end
end
