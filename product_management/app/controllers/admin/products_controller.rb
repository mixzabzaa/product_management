class Admin::ProductsController < Admin::AppController
  before_action :set_product_group
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = @product_group.products
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    @product.product_group = @product_group
    @product.business_client = @main_business_client
    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_product_group_products_path(business_client_key: @main_business_client.key, product_group_uid: @product_group.uid), notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to admin_product_group_product_path(business_client_key: @main_business_client.key, product_group_uid: @product_group.uid, uid: @product.uid), notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_group
      @product_group = @main_business_client.product_groups.find_by_uid(params[:product_group_uid])
      raise "NotFoud" if @product_group.nil?
    end

    def set_product
      @product = @product_group.products.find_by_uid(params[:uid])
      raise "NotFoud" if @product.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      p = params.require(:product).permit(:name, :price, :quantity)
      p[:tag_criteria] = params[:product][:tag_criteria]
      p
    end

end
