require "rails_helper"

RSpec.describe TimeIntervalProductsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/time_interval_products").to route_to("time_interval_products#index")
    end

    it "routes to #new" do
      expect(:get => "/time_interval_products/new").to route_to("time_interval_products#new")
    end

    it "routes to #show" do
      expect(:get => "/time_interval_products/1").to route_to("time_interval_products#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/time_interval_products/1/edit").to route_to("time_interval_products#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/time_interval_products").to route_to("time_interval_products#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/time_interval_products/1").to route_to("time_interval_products#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/time_interval_products/1").to route_to("time_interval_products#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/time_interval_products/1").to route_to("time_interval_products#destroy", :id => "1")
    end

  end
end
