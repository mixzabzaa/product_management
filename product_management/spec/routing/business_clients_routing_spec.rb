require "rails_helper"

RSpec.describe BusinessClientsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/business_clients").to route_to("business_clients#index")
    end

    it "routes to #new" do
      expect(:get => "/business_clients/new").to route_to("business_clients#new")
    end

    it "routes to #show" do
      expect(:get => "/business_clients/1").to route_to("business_clients#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/business_clients/1/edit").to route_to("business_clients#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/business_clients").to route_to("business_clients#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/business_clients/1").to route_to("business_clients#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/business_clients/1").to route_to("business_clients#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/business_clients/1").to route_to("business_clients#destroy", :id => "1")
    end

  end
end
