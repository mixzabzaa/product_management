require "rails_helper"

RSpec.describe OrderProductAddOnsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/order_product_add_ons").to route_to("order_product_add_ons#index")
    end

    it "routes to #new" do
      expect(:get => "/order_product_add_ons/new").to route_to("order_product_add_ons#new")
    end

    it "routes to #show" do
      expect(:get => "/order_product_add_ons/1").to route_to("order_product_add_ons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/order_product_add_ons/1/edit").to route_to("order_product_add_ons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/order_product_add_ons").to route_to("order_product_add_ons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/order_product_add_ons/1").to route_to("order_product_add_ons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/order_product_add_ons/1").to route_to("order_product_add_ons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/order_product_add_ons/1").to route_to("order_product_add_ons#destroy", :id => "1")
    end

  end
end
