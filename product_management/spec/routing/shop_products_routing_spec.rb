require "rails_helper"

RSpec.describe ShopProductsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/shop_products").to route_to("shop_products#index")
    end

    it "routes to #new" do
      expect(:get => "/shop_products/new").to route_to("shop_products#new")
    end

    it "routes to #show" do
      expect(:get => "/shop_products/1").to route_to("shop_products#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/shop_products/1/edit").to route_to("shop_products#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/shop_products").to route_to("shop_products#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/shop_products/1").to route_to("shop_products#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/shop_products/1").to route_to("shop_products#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/shop_products/1").to route_to("shop_products#destroy", :id => "1")
    end

  end
end
