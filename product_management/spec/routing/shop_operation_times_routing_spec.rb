require "rails_helper"

RSpec.describe ShopOperationTimesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/shop_operation_times").to route_to("shop_operation_times#index")
    end

    it "routes to #new" do
      expect(:get => "/shop_operation_times/new").to route_to("shop_operation_times#new")
    end

    it "routes to #show" do
      expect(:get => "/shop_operation_times/1").to route_to("shop_operation_times#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/shop_operation_times/1/edit").to route_to("shop_operation_times#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/shop_operation_times").to route_to("shop_operation_times#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/shop_operation_times/1").to route_to("shop_operation_times#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/shop_operation_times/1").to route_to("shop_operation_times#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/shop_operation_times/1").to route_to("shop_operation_times#destroy", :id => "1")
    end

  end
end
