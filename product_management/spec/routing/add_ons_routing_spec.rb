require "rails_helper"

RSpec.describe AddOnsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/add_ons").to route_to("add_ons#index")
    end

    it "routes to #new" do
      expect(:get => "/add_ons/new").to route_to("add_ons#new")
    end

    it "routes to #show" do
      expect(:get => "/add_ons/1").to route_to("add_ons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/add_ons/1/edit").to route_to("add_ons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/add_ons").to route_to("add_ons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/add_ons/1").to route_to("add_ons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/add_ons/1").to route_to("add_ons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/add_ons/1").to route_to("add_ons#destroy", :id => "1")
    end

  end
end
