require "rails_helper"

RSpec.describe ProductAddOnsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/product_add_ons").to route_to("product_add_ons#index")
    end

    it "routes to #new" do
      expect(:get => "/product_add_ons/new").to route_to("product_add_ons#new")
    end

    it "routes to #show" do
      expect(:get => "/product_add_ons/1").to route_to("product_add_ons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/product_add_ons/1/edit").to route_to("product_add_ons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/product_add_ons").to route_to("product_add_ons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/product_add_ons/1").to route_to("product_add_ons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/product_add_ons/1").to route_to("product_add_ons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/product_add_ons/1").to route_to("product_add_ons#destroy", :id => "1")
    end

  end
end
