require 'rails_helper'

RSpec.describe "employees/index", type: :view do
  before(:each) do
    assign(:employees, [
      Employee.create!(
        :uid => "MyText",
        :title => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :telephone => "MyText",
        :shop => nil
      ),
      Employee.create!(
        :uid => "MyText",
        :title => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :telephone => "MyText",
        :shop => nil
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
