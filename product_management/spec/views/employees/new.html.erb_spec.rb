require 'rails_helper'

RSpec.describe "employees/new", type: :view do
  before(:each) do
    assign(:employee, Employee.new(
      :uid => "MyText",
      :title => "MyText",
      :first_name => "MyText",
      :last_name => "MyText",
      :telephone => "MyText",
      :shop => nil
    ))
  end

  it "renders new employee form" do
    render

    assert_select "form[action=?][method=?]", employees_path, "post" do

      assert_select "textarea#employee_uid[name=?]", "employee[uid]"

      assert_select "textarea#employee_title[name=?]", "employee[title]"

      assert_select "textarea#employee_first_name[name=?]", "employee[first_name]"

      assert_select "textarea#employee_last_name[name=?]", "employee[last_name]"

      assert_select "textarea#employee_telephone[name=?]", "employee[telephone]"

      assert_select "input#employee_shop_id[name=?]", "employee[shop_id]"
    end
  end
end
