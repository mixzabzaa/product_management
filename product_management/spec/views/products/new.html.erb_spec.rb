require 'rails_helper'

RSpec.describe "products/new", type: :view do
  before(:each) do
    assign(:product, Product.new(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :quantity => 1,
      :business_client => nil,
      :product_group => nil,
      :tag_criteria => ""
    ))
  end

  it "renders new product form" do
    render

    assert_select "form[action=?][method=?]", products_path, "post" do

      assert_select "textarea#product_uid[name=?]", "product[uid]"

      assert_select "textarea#product_name[name=?]", "product[name]"

      assert_select "input#product_price[name=?]", "product[price]"

      assert_select "input#product_quantity[name=?]", "product[quantity]"

      assert_select "input#product_business_client_id[name=?]", "product[business_client_id]"

      assert_select "input#product_product_group_id[name=?]", "product[product_group_id]"

      assert_select "input#product_tag_criteria[name=?]", "product[tag_criteria]"
    end
  end
end
