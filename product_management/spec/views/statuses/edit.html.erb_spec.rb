require 'rails_helper'

RSpec.describe "statuses/edit", type: :view do
  before(:each) do
    @status = assign(:status, Status.create!(
      :uid => "MyText",
      :shop => nil,
      :name => "MyText",
      :notification => "MyText",
      :step => 1
    ))
  end

  it "renders the edit status form" do
    render

    assert_select "form[action=?][method=?]", status_path(@status), "post" do

      assert_select "textarea#status_uid[name=?]", "status[uid]"

      assert_select "input#status_shop_id[name=?]", "status[shop_id]"

      assert_select "textarea#status_name[name=?]", "status[name]"

      assert_select "textarea#status_notification[name=?]", "status[notification]"

      assert_select "input#status_step[name=?]", "status[step]"
    end
  end
end
