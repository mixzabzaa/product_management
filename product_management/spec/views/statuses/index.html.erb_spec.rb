require 'rails_helper'

RSpec.describe "statuses/index", type: :view do
  before(:each) do
    assign(:statuses, [
      Status.create!(
        :uid => "MyText",
        :shop => nil,
        :name => "MyText",
        :notification => "MyText",
        :step => 2
      ),
      Status.create!(
        :uid => "MyText",
        :shop => nil,
        :name => "MyText",
        :notification => "MyText",
        :step => 2
      )
    ])
  end

  it "renders a list of statuses" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
