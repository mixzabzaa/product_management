require 'rails_helper'

RSpec.describe "statuses/new", type: :view do
  before(:each) do
    assign(:status, Status.new(
      :uid => "MyText",
      :shop => nil,
      :name => "MyText",
      :notification => "MyText",
      :step => 1
    ))
  end

  it "renders new status form" do
    render

    assert_select "form[action=?][method=?]", statuses_path, "post" do

      assert_select "textarea#status_uid[name=?]", "status[uid]"

      assert_select "input#status_shop_id[name=?]", "status[shop_id]"

      assert_select "textarea#status_name[name=?]", "status[name]"

      assert_select "textarea#status_notification[name=?]", "status[notification]"

      assert_select "input#status_step[name=?]", "status[step]"
    end
  end
end
