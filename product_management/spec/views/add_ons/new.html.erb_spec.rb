require 'rails_helper'

RSpec.describe "add_ons/new", type: :view do
  before(:each) do
    assign(:add_on, AddOn.new(
      :uid => "MyText",
      :name => "MyText",
      :price => 1,
      :business_client => nil
    ))
  end

  it "renders new add_on form" do
    render

    assert_select "form[action=?][method=?]", add_ons_path, "post" do

      assert_select "textarea#add_on_uid[name=?]", "add_on[uid]"

      assert_select "textarea#add_on_name[name=?]", "add_on[name]"

      assert_select "input#add_on_price[name=?]", "add_on[price]"

      assert_select "input#add_on_business_client_id[name=?]", "add_on[business_client_id]"
    end
  end
end
