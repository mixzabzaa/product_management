require 'rails_helper'

RSpec.describe "add_ons/show", type: :view do
  before(:each) do
    @add_on = assign(:add_on, AddOn.create!(
      :uid => "MyText",
      :name => "MyText",
      :price => 2,
      :business_client => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
  end
end
