require 'rails_helper'

RSpec.describe "add_ons/index", type: :view do
  before(:each) do
    assign(:add_ons, [
      AddOn.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => 2,
        :business_client => nil
      ),
      AddOn.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => 2,
        :business_client => nil
      )
    ])
  end

  it "renders a list of add_ons" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
