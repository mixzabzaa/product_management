require 'rails_helper'

RSpec.describe "product_add_ons/edit", type: :view do
  before(:each) do
    @product_add_on = assign(:product_add_on, ProductAddOn.create!(
      :uid => "MyText",
      :price => "9.99",
      :product => nil,
      :add_on => nil
    ))
  end

  it "renders the edit product_add_on form" do
    render

    assert_select "form[action=?][method=?]", product_add_on_path(@product_add_on), "post" do

      assert_select "textarea#product_add_on_uid[name=?]", "product_add_on[uid]"

      assert_select "input#product_add_on_price[name=?]", "product_add_on[price]"

      assert_select "input#product_add_on_product_id[name=?]", "product_add_on[product_id]"

      assert_select "input#product_add_on_add_on_id[name=?]", "product_add_on[add_on_id]"
    end
  end
end
