require 'rails_helper'

RSpec.describe "product_add_ons/index", type: :view do
  before(:each) do
    assign(:product_add_ons, [
      ProductAddOn.create!(
        :uid => "MyText",
        :price => "9.99",
        :product => nil,
        :add_on => nil
      ),
      ProductAddOn.create!(
        :uid => "MyText",
        :price => "9.99",
        :product => nil,
        :add_on => nil
      )
    ])
  end

  it "renders a list of product_add_ons" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
