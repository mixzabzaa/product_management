require 'rails_helper'

RSpec.describe "product_add_ons/show", type: :view do
  before(:each) do
    @product_add_on = assign(:product_add_on, ProductAddOn.create!(
      :uid => "MyText",
      :price => "9.99",
      :product => nil,
      :add_on => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
