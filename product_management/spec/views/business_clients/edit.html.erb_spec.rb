require 'rails_helper'

RSpec.describe "business_clients/edit", type: :view do
  before(:each) do
    @business_client = assign(:business_client, BusinessClient.create!(
      :uid => "MyText",
      :name => "MyText",
      :key => "MyText"
    ))
  end

  it "renders the edit business_client form" do
    render

    assert_select "form[action=?][method=?]", business_client_path(@business_client), "post" do

      assert_select "textarea#business_client_uid[name=?]", "business_client[uid]"

      assert_select "textarea#business_client_name[name=?]", "business_client[name]"

      assert_select "textarea#business_client_key[name=?]", "business_client[key]"
    end
  end
end
