require 'rails_helper'

RSpec.describe "business_clients/show", type: :view do
  before(:each) do
    @business_client = assign(:business_client, BusinessClient.create!(
      :uid => "MyText",
      :name => "MyText",
      :key => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
