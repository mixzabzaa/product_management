require 'rails_helper'

RSpec.describe "business_clients/index", type: :view do
  before(:each) do
    assign(:business_clients, [
      BusinessClient.create!(
        :uid => "MyText",
        :name => "MyText",
        :key => "MyText"
      ),
      BusinessClient.create!(
        :uid => "MyText",
        :name => "MyText",
        :key => "MyText"
      )
    ])
  end

  it "renders a list of business_clients" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
