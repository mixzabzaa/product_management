require 'rails_helper'

RSpec.describe "business_clients/new", type: :view do
  before(:each) do
    assign(:business_client, BusinessClient.new(
      :uid => "MyText",
      :name => "MyText",
      :key => "MyText"
    ))
  end

  it "renders new business_client form" do
    render

    assert_select "form[action=?][method=?]", business_clients_path, "post" do

      assert_select "textarea#business_client_uid[name=?]", "business_client[uid]"

      assert_select "textarea#business_client_name[name=?]", "business_client[name]"

      assert_select "textarea#business_client_key[name=?]", "business_client[key]"
    end
  end
end
