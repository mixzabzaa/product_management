require 'rails_helper'

RSpec.describe "order_products/new", type: :view do
  before(:each) do
    assign(:order_product, OrderProduct.new(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :quantity => 1,
      :order => nil,
      :shop_product => nil
    ))
  end

  it "renders new order_product form" do
    render

    assert_select "form[action=?][method=?]", order_products_path, "post" do

      assert_select "textarea#order_product_uid[name=?]", "order_product[uid]"

      assert_select "textarea#order_product_name[name=?]", "order_product[name]"

      assert_select "input#order_product_price[name=?]", "order_product[price]"

      assert_select "input#order_product_quantity[name=?]", "order_product[quantity]"

      assert_select "input#order_product_order_id[name=?]", "order_product[order_id]"

      assert_select "input#order_product_shop_product_id[name=?]", "order_product[shop_product_id]"
    end
  end
end
