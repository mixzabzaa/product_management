require 'rails_helper'

RSpec.describe "order_products/show", type: :view do
  before(:each) do
    @order_product = assign(:order_product, OrderProduct.create!(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :quantity => 2,
      :order => nil,
      :shop_product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
