require 'rails_helper'

RSpec.describe "order_products/index", type: :view do
  before(:each) do
    assign(:order_products, [
      OrderProduct.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => "9.99",
        :quantity => 2,
        :order => nil,
        :shop_product => nil
      ),
      OrderProduct.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => "9.99",
        :quantity => 2,
        :order => nil,
        :shop_product => nil
      )
    ])
  end

  it "renders a list of order_products" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
