require 'rails_helper'

RSpec.describe "shop_operation_times/show", type: :view do
  before(:each) do
    @shop_operation_time = assign(:shop_operation_time, ShopOperationTime.create!(
      :uid => "MyText",
      :shop => nil,
      :name => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
  end
end
