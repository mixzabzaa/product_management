require 'rails_helper'

RSpec.describe "shop_operation_times/new", type: :view do
  before(:each) do
    assign(:shop_operation_time, ShopOperationTime.new(
      :uid => "MyText",
      :shop => nil,
      :name => "MyText"
    ))
  end

  it "renders new shop_operation_time form" do
    render

    assert_select "form[action=?][method=?]", shop_operation_times_path, "post" do

      assert_select "textarea#shop_operation_time_uid[name=?]", "shop_operation_time[uid]"

      assert_select "input#shop_operation_time_shop_id[name=?]", "shop_operation_time[shop_id]"

      assert_select "textarea#shop_operation_time_name[name=?]", "shop_operation_time[name]"
    end
  end
end
