require 'rails_helper'

RSpec.describe "shop_operation_times/index", type: :view do
  before(:each) do
    assign(:shop_operation_times, [
      ShopOperationTime.create!(
        :uid => "MyText",
        :shop => nil,
        :name => "MyText"
      ),
      ShopOperationTime.create!(
        :uid => "MyText",
        :shop => nil,
        :name => "MyText"
      )
    ])
  end

  it "renders a list of shop_operation_times" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
