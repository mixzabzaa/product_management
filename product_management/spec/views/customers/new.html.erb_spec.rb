require 'rails_helper'

RSpec.describe "customers/new", type: :view do
  before(:each) do
    assign(:customer, Customer.new(
      :uid => "MyText",
      :title => "MyText",
      :first_name => "MyText",
      :last_name => "MyText",
      :email => "MyText",
      :telephone => "MyText",
      :address => "MyText",
      :business_client => nil
    ))
  end

  it "renders new customer form" do
    render

    assert_select "form[action=?][method=?]", customers_path, "post" do

      assert_select "textarea#customer_uid[name=?]", "customer[uid]"

      assert_select "textarea#customer_title[name=?]", "customer[title]"

      assert_select "textarea#customer_first_name[name=?]", "customer[first_name]"

      assert_select "textarea#customer_last_name[name=?]", "customer[last_name]"

      assert_select "textarea#customer_email[name=?]", "customer[email]"

      assert_select "textarea#customer_telephone[name=?]", "customer[telephone]"

      assert_select "textarea#customer_address[name=?]", "customer[address]"

      assert_select "input#customer_business_client_id[name=?]", "customer[business_client_id]"
    end
  end
end
