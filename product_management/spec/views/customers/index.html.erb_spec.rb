require 'rails_helper'

RSpec.describe "customers/index", type: :view do
  before(:each) do
    assign(:customers, [
      Customer.create!(
        :uid => "MyText",
        :title => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :email => "MyText",
        :telephone => "MyText",
        :address => "MyText",
        :business_client => nil
      ),
      Customer.create!(
        :uid => "MyText",
        :title => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :email => "MyText",
        :telephone => "MyText",
        :address => "MyText",
        :business_client => nil
      )
    ])
  end

  it "renders a list of customers" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
