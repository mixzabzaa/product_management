require 'rails_helper'

RSpec.describe "admins/new", type: :view do
  before(:each) do
    assign(:admin, Admin.new(
      :uid => "MyText",
      :first_name => "MyText",
      :last_name => "MyText",
      :business_client => nil
    ))
  end

  it "renders new admin form" do
    render

    assert_select "form[action=?][method=?]", admins_path, "post" do

      assert_select "textarea#admin_uid[name=?]", "admin[uid]"

      assert_select "textarea#admin_first_name[name=?]", "admin[first_name]"

      assert_select "textarea#admin_last_name[name=?]", "admin[last_name]"

      assert_select "input#admin_business_client_id[name=?]", "admin[business_client_id]"
    end
  end
end
