require 'rails_helper'

RSpec.describe "admins/index", type: :view do
  before(:each) do
    assign(:admins, [
      Admin.create!(
        :uid => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :business_client => nil
      ),
      Admin.create!(
        :uid => "MyText",
        :first_name => "MyText",
        :last_name => "MyText",
        :business_client => nil
      )
    ])
  end

  it "renders a list of admins" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
