require 'rails_helper'

RSpec.describe "admins/edit", type: :view do
  before(:each) do
    @admin = assign(:admin, Admin.create!(
      :uid => "MyText",
      :first_name => "MyText",
      :last_name => "MyText",
      :business_client => nil
    ))
  end

  it "renders the edit admin form" do
    render

    assert_select "form[action=?][method=?]", admin_path(@admin), "post" do

      assert_select "textarea#admin_uid[name=?]", "admin[uid]"

      assert_select "textarea#admin_first_name[name=?]", "admin[first_name]"

      assert_select "textarea#admin_last_name[name=?]", "admin[last_name]"

      assert_select "input#admin_business_client_id[name=?]", "admin[business_client_id]"
    end
  end
end
