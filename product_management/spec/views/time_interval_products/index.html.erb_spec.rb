require 'rails_helper'

RSpec.describe "time_interval_products/index", type: :view do
  before(:each) do
    assign(:time_interval_products, [
      TimeIntervalProduct.create!(
        :uid => "MyText",
        :name => "MyText",
        :shop => nil,
        :shop_product => nil
      ),
      TimeIntervalProduct.create!(
        :uid => "MyText",
        :name => "MyText",
        :shop => nil,
        :shop_product => nil
      )
    ])
  end

  it "renders a list of time_interval_products" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
