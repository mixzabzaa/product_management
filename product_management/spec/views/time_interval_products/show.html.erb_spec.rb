require 'rails_helper'

RSpec.describe "time_interval_products/show", type: :view do
  before(:each) do
    @time_interval_product = assign(:time_interval_product, TimeIntervalProduct.create!(
      :uid => "MyText",
      :name => "MyText",
      :shop => nil,
      :shop_product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
