require 'rails_helper'

RSpec.describe "time_interval_products/edit", type: :view do
  before(:each) do
    @time_interval_product = assign(:time_interval_product, TimeIntervalProduct.create!(
      :uid => "MyText",
      :name => "MyText",
      :shop => nil,
      :shop_product => nil
    ))
  end

  it "renders the edit time_interval_product form" do
    render

    assert_select "form[action=?][method=?]", time_interval_product_path(@time_interval_product), "post" do

      assert_select "textarea#time_interval_product_uid[name=?]", "time_interval_product[uid]"

      assert_select "textarea#time_interval_product_name[name=?]", "time_interval_product[name]"

      assert_select "input#time_interval_product_shop_id[name=?]", "time_interval_product[shop_id]"

      assert_select "input#time_interval_product_shop_product_id[name=?]", "time_interval_product[shop_product_id]"
    end
  end
end
