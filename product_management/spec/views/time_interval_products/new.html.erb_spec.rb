require 'rails_helper'

RSpec.describe "time_interval_products/new", type: :view do
  before(:each) do
    assign(:time_interval_product, TimeIntervalProduct.new(
      :uid => "MyText",
      :name => "MyText",
      :shop => nil,
      :shop_product => nil
    ))
  end

  it "renders new time_interval_product form" do
    render

    assert_select "form[action=?][method=?]", time_interval_products_path, "post" do

      assert_select "textarea#time_interval_product_uid[name=?]", "time_interval_product[uid]"

      assert_select "textarea#time_interval_product_name[name=?]", "time_interval_product[name]"

      assert_select "input#time_interval_product_shop_id[name=?]", "time_interval_product[shop_id]"

      assert_select "input#time_interval_product_shop_product_id[name=?]", "time_interval_product[shop_product_id]"
    end
  end
end
