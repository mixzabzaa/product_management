require 'rails_helper'

RSpec.describe "shops/new", type: :view do
  before(:each) do
    assign(:shop, Shop.new(
      :uid => "MyText",
      :name => "MyText",
      :business_client => nil,
      :address => "MyText",
      :telephone => "MyText"
    ))
  end

  it "renders new shop form" do
    render

    assert_select "form[action=?][method=?]", shops_path, "post" do

      assert_select "textarea#shop_uid[name=?]", "shop[uid]"

      assert_select "textarea#shop_name[name=?]", "shop[name]"

      assert_select "input#shop_business_client_id[name=?]", "shop[business_client_id]"

      assert_select "textarea#shop_address[name=?]", "shop[address]"

      assert_select "textarea#shop_telephone[name=?]", "shop[telephone]"
    end
  end
end
