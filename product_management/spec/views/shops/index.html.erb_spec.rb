require 'rails_helper'

RSpec.describe "shops/index", type: :view do
  before(:each) do
    assign(:shops, [
      Shop.create!(
        :uid => "MyText",
        :name => "MyText",
        :business_client => nil,
        :address => "MyText",
        :telephone => "MyText"
      ),
      Shop.create!(
        :uid => "MyText",
        :name => "MyText",
        :business_client => nil,
        :address => "MyText",
        :telephone => "MyText"
      )
    ])
  end

  it "renders a list of shops" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
