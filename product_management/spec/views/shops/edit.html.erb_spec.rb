require 'rails_helper'

RSpec.describe "shops/edit", type: :view do
  before(:each) do
    @shop = assign(:shop, Shop.create!(
      :uid => "MyText",
      :name => "MyText",
      :business_client => nil,
      :address => "MyText",
      :telephone => "MyText"
    ))
  end

  it "renders the edit shop form" do
    render

    assert_select "form[action=?][method=?]", shop_path(@shop), "post" do

      assert_select "textarea#shop_uid[name=?]", "shop[uid]"

      assert_select "textarea#shop_name[name=?]", "shop[name]"

      assert_select "input#shop_business_client_id[name=?]", "shop[business_client_id]"

      assert_select "textarea#shop_address[name=?]", "shop[address]"

      assert_select "textarea#shop_telephone[name=?]", "shop[telephone]"
    end
  end
end
