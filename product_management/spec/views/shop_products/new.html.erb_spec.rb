require 'rails_helper'

RSpec.describe "shop_products/new", type: :view do
  before(:each) do
    assign(:shop_product, ShopProduct.new(
      :uid => "MyText",
      :price => "9.99",
      :quantity => 1,
      :shop => nil,
      :product => nil
    ))
  end

  it "renders new shop_product form" do
    render

    assert_select "form[action=?][method=?]", shop_products_path, "post" do

      assert_select "textarea#shop_product_uid[name=?]", "shop_product[uid]"

      assert_select "input#shop_product_price[name=?]", "shop_product[price]"

      assert_select "input#shop_product_quantity[name=?]", "shop_product[quantity]"

      assert_select "input#shop_product_shop_id[name=?]", "shop_product[shop_id]"

      assert_select "input#shop_product_product_id[name=?]", "shop_product[product_id]"
    end
  end
end
