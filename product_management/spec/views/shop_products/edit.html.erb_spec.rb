require 'rails_helper'

RSpec.describe "shop_products/edit", type: :view do
  before(:each) do
    @shop_product = assign(:shop_product, ShopProduct.create!(
      :uid => "MyText",
      :price => "9.99",
      :quantity => 1,
      :shop => nil,
      :product => nil
    ))
  end

  it "renders the edit shop_product form" do
    render

    assert_select "form[action=?][method=?]", shop_product_path(@shop_product), "post" do

      assert_select "textarea#shop_product_uid[name=?]", "shop_product[uid]"

      assert_select "input#shop_product_price[name=?]", "shop_product[price]"

      assert_select "input#shop_product_quantity[name=?]", "shop_product[quantity]"

      assert_select "input#shop_product_shop_id[name=?]", "shop_product[shop_id]"

      assert_select "input#shop_product_product_id[name=?]", "shop_product[product_id]"
    end
  end
end
