require 'rails_helper'

RSpec.describe "shop_products/show", type: :view do
  before(:each) do
    @shop_product = assign(:shop_product, ShopProduct.create!(
      :uid => "MyText",
      :price => "9.99",
      :quantity => 2,
      :shop => nil,
      :product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
