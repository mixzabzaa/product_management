require 'rails_helper'

RSpec.describe "shop_products/index", type: :view do
  before(:each) do
    assign(:shop_products, [
      ShopProduct.create!(
        :uid => "MyText",
        :price => "9.99",
        :quantity => 2,
        :shop => nil,
        :product => nil
      ),
      ShopProduct.create!(
        :uid => "MyText",
        :price => "9.99",
        :quantity => 2,
        :shop => nil,
        :product => nil
      )
    ])
  end

  it "renders a list of shop_products" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
