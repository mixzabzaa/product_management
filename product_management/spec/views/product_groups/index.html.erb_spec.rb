require 'rails_helper'

RSpec.describe "product_groups/index", type: :view do
  before(:each) do
    assign(:product_groups, [
      ProductGroup.create!(
        :uid => "MyText",
        :business_client => nil,
        :name => "MyText"
      ),
      ProductGroup.create!(
        :uid => "MyText",
        :business_client => nil,
        :name => "MyText"
      )
    ])
  end

  it "renders a list of product_groups" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
