require 'rails_helper'

RSpec.describe "product_groups/new", type: :view do
  before(:each) do
    assign(:product_group, ProductGroup.new(
      :uid => "MyText",
      :business_client => nil,
      :name => "MyText"
    ))
  end

  it "renders new product_group form" do
    render

    assert_select "form[action=?][method=?]", product_groups_path, "post" do

      assert_select "textarea#product_group_uid[name=?]", "product_group[uid]"

      assert_select "input#product_group_business_client_id[name=?]", "product_group[business_client_id]"

      assert_select "textarea#product_group_name[name=?]", "product_group[name]"
    end
  end
end
