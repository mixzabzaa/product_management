require 'rails_helper'

RSpec.describe "order_product_add_ons/index", type: :view do
  before(:each) do
    assign(:order_product_add_ons, [
      OrderProductAddOn.create!(
        :uid => "MyText",
        :shop_product_add_on => nil,
        :order_product => nil
      ),
      OrderProductAddOn.create!(
        :uid => "MyText",
        :shop_product_add_on => nil,
        :order_product => nil
      )
    ])
  end

  it "renders a list of order_product_add_ons" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
