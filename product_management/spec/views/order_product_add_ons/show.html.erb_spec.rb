require 'rails_helper'

RSpec.describe "order_product_add_ons/show", type: :view do
  before(:each) do
    @order_product_add_on = assign(:order_product_add_on, OrderProductAddOn.create!(
      :uid => "MyText",
      :shop_product_add_on => nil,
      :order_product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
