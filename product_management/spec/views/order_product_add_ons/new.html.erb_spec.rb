require 'rails_helper'

RSpec.describe "order_product_add_ons/new", type: :view do
  before(:each) do
    assign(:order_product_add_on, OrderProductAddOn.new(
      :uid => "MyText",
      :shop_product_add_on => nil,
      :order_product => nil
    ))
  end

  it "renders new order_product_add_on form" do
    render

    assert_select "form[action=?][method=?]", order_product_add_ons_path, "post" do

      assert_select "textarea#order_product_add_on_uid[name=?]", "order_product_add_on[uid]"

      assert_select "input#order_product_add_on_shop_product_add_on_id[name=?]", "order_product_add_on[shop_product_add_on_id]"

      assert_select "input#order_product_add_on_order_product_id[name=?]", "order_product_add_on[order_product_id]"
    end
  end
end
