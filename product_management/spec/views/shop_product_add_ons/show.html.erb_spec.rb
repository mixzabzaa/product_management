require 'rails_helper'

RSpec.describe "shop_product_add_ons/show", type: :view do
  before(:each) do
    @shop_product_add_on = assign(:shop_product_add_on, ShopProductAddOn.create!(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :product_add_on => nil,
      :shop_product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
