require 'rails_helper'

RSpec.describe "shop_product_add_ons/index", type: :view do
  before(:each) do
    assign(:shop_product_add_ons, [
      ShopProductAddOn.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => "9.99",
        :product_add_on => nil,
        :shop_product => nil
      ),
      ShopProductAddOn.create!(
        :uid => "MyText",
        :name => "MyText",
        :price => "9.99",
        :product_add_on => nil,
        :shop_product => nil
      )
    ])
  end

  it "renders a list of shop_product_add_ons" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
