require 'rails_helper'

RSpec.describe "shop_product_add_ons/new", type: :view do
  before(:each) do
    assign(:shop_product_add_on, ShopProductAddOn.new(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :product_add_on => nil,
      :shop_product => nil
    ))
  end

  it "renders new shop_product_add_on form" do
    render

    assert_select "form[action=?][method=?]", shop_product_add_ons_path, "post" do

      assert_select "textarea#shop_product_add_on_uid[name=?]", "shop_product_add_on[uid]"

      assert_select "textarea#shop_product_add_on_name[name=?]", "shop_product_add_on[name]"

      assert_select "input#shop_product_add_on_price[name=?]", "shop_product_add_on[price]"

      assert_select "input#shop_product_add_on_product_add_on_id[name=?]", "shop_product_add_on[product_add_on_id]"

      assert_select "input#shop_product_add_on_shop_product_id[name=?]", "shop_product_add_on[shop_product_id]"
    end
  end
end
