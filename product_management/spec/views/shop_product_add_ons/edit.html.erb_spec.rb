require 'rails_helper'

RSpec.describe "shop_product_add_ons/edit", type: :view do
  before(:each) do
    @shop_product_add_on = assign(:shop_product_add_on, ShopProductAddOn.create!(
      :uid => "MyText",
      :name => "MyText",
      :price => "9.99",
      :product_add_on => nil,
      :shop_product => nil
    ))
  end

  it "renders the edit shop_product_add_on form" do
    render

    assert_select "form[action=?][method=?]", shop_product_add_on_path(@shop_product_add_on), "post" do

      assert_select "textarea#shop_product_add_on_uid[name=?]", "shop_product_add_on[uid]"

      assert_select "textarea#shop_product_add_on_name[name=?]", "shop_product_add_on[name]"

      assert_select "input#shop_product_add_on_price[name=?]", "shop_product_add_on[price]"

      assert_select "input#shop_product_add_on_product_add_on_id[name=?]", "shop_product_add_on[product_add_on_id]"

      assert_select "input#shop_product_add_on_shop_product_id[name=?]", "shop_product_add_on[shop_product_id]"
    end
  end
end
