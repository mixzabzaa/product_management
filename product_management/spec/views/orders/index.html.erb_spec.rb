require 'rails_helper'

RSpec.describe "orders/index", type: :view do
  before(:each) do
    assign(:orders, [
      Order.create!(
        :uid => "MyText",
        :total_price => "9.99",
        :shop => nil,
        :customer => nil,
        :status => nil,
        :description => "MyText"
      ),
      Order.create!(
        :uid => "MyText",
        :total_price => "9.99",
        :shop => nil,
        :customer => nil,
        :status => nil,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of orders" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
