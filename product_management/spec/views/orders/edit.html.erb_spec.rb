require 'rails_helper'

RSpec.describe "orders/edit", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :uid => "MyText",
      :total_price => "9.99",
      :shop => nil,
      :customer => nil,
      :status => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit order form" do
    render

    assert_select "form[action=?][method=?]", order_path(@order), "post" do

      assert_select "textarea#order_uid[name=?]", "order[uid]"

      assert_select "input#order_total_price[name=?]", "order[total_price]"

      assert_select "input#order_shop_id[name=?]", "order[shop_id]"

      assert_select "input#order_customer_id[name=?]", "order[customer_id]"

      assert_select "input#order_status_id[name=?]", "order[status_id]"

      assert_select "textarea#order_description[name=?]", "order[description]"
    end
  end
end
