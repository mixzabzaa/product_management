Rails.application.configure do
  config.cache_classes = false
  config.eager_load = false
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.action_mailer.raise_delivery_errors = false
  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load
  config.assets.debug = true
  config.assets.digest = true
  config.assets.raise_runtime_errors = true
  config.action_view.raise_on_missing_translations = false

  config.action_mailer.default_url_options = { host: 'localhost', port: 4015 }

  config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins '*'
      resource '*', :headers => :any, :methods => [:get, :post, :put, :patch]
    end
  end

  ns = "#{Rails.application.class.to_s.underscore.split('/').first}_#{Rails.env}_cache_store"
  config.cache_store = :redis_store, {
                                    host: Settings.redis_uri,
                                    port: Settings.redis_port,
                                    namespace: ns,
                                    expires_in: 90.minutes
                                 }
  config.action_mailer.delivery_method = :mailgun
  config.action_mailer.mailgun_settings = {
         api_key: 'key-83c0e6aaa430964179903e2abc17081a',
         domain: 'no-reply.cloudandthing.com'
  }
  config.action_mailer.perform_deliveries = true

end
