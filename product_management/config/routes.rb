Rails.application.routes.draw do

  # devise_for :employees
  devise_for :admins, controllers: { sessions: 'admins/sessions', passwords:'admins/passwords', confirmations:'admins/confirmations', unlocks:'admins/unlocks'}

  scope "(:locale)" do
    root to: "admin/admins#index"
    namespace :admin, path: "admin/:business_client_key" do
      root to: "admins#index"
      resources :admins, param: :uid
      resources :product_groups, param: :uid do
        resources :products, param: :uid do
          resources :product_add_ons, param: :uid
        end
      end
      resources :shops, param: :uid do
        resources :shop_operation_times, param: :uid
        resources :time_interval_products, param: :uid
        resources :employees, param: :uid
        resources :statuses, param: :uid
        resources :shop_products, param: :uid do
          resources :shop_product_add_ons, param: :uid
        end
      end

      resources :order_product_add_ons, param: :uid
      resources :order_products, param: :uid
      resources :customers, param: :uid do
        resources :orders, param: :uid
      end
      resources :add_ons, param: :uid
      resources :business_clients, param: :uid
    end

    namespace :customer, path: "customer/:business_client_key" do
      resources :customers, param: :uid do
        resources :orders, param: :uid
      end

    end
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
