rails g scaffold BusinessClient uid:text:uniq:index name:text key:text:uniq:index
rails g scaffold Admin uid:text:uniq:index first_name:text last_name:text business_client:references
rails g scaffold Shop uid:text:uniq:index name:text business_client:references address:text telephone:text
rails g scaffold Employee uid:text:uniq:index title:text first_name:text last_name:text telephone:text shop:references
rails g scaffold ProductGroup uid:text:uniq:index business_client:references name:text
rails g scaffold Product uid:text:uniq:index name:text price:decimal quantity:integer business_client:references product_group:references tag_criteria:jsonb
rails g scaffold AddOn uid:text:uniq:index name:text price:integer business_client:references
rails g scaffold Customer uid:text:uniq:index title:text first_name:text last_name:text email:text telephone:text address:text business_client:references
rails g scaffold ShopProduct uid:text:uniq:index price:decimal quantity:integer shop:references product:references
rails g scaffold TimeIntervalProduct uid:text:uniq:index name:text start_time:datetime end_time:datetime shop:references shop_product:references
rails g scaffold ShopOperationTime uid:text:uniq:index shop:references name:text start_time:datetime end_time:datetime
rails g scaffold Status uid:text:uniq:index shop:references name:text notification:text step:integer
rails g scaffold ProductAddOn uid:text:uniq:index price:decimal product:references add_on:references
rails g scaffold ShopProductAddOn uid:text:uniq:index name:text price:decimal product_add_on:references shop_product:references
rails g scaffold Order uid:text:uniq:index total_price:decimal shop:references customer:references status:references description:text
rails g scaffold OrderProduct uid:text:uniq:index name:text price:decimal quantity:integer order:references shop_product:references
rails g scaffold OrderProductAddOn uid:text:uniq:index shop_product_add_on:references order_product:references
