# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170223061126) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "add_ons", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.integer  "price"
    t.integer  "business_client_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_client_id"], name: "index_add_ons_on_business_client_id", using: :btree
    t.index ["uid"], name: "index_add_ons_on_uid", unique: true, using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.text     "uid"
    t.text     "first_name"
    t.text     "last_name"
    t.integer  "business_client_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.index ["business_client_id"], name: "index_admins_on_business_client_id", using: :btree
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
    t.index ["uid"], name: "index_admins_on_uid", unique: true, using: :btree
  end

  create_table "business_clients", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.text     "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_business_clients_on_key", unique: true, using: :btree
    t.index ["uid"], name: "index_business_clients_on_uid", unique: true, using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.text     "uid"
    t.text     "title"
    t.text     "first_name"
    t.text     "last_name"
    t.text     "email"
    t.text     "telephone"
    t.text     "address"
    t.integer  "business_client_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_client_id"], name: "index_customers_on_business_client_id", using: :btree
    t.index ["uid"], name: "index_customers_on_uid", unique: true, using: :btree
  end

  create_table "employees", force: :cascade do |t|
    t.text     "uid"
    t.text     "title"
    t.text     "first_name"
    t.text     "last_name"
    t.text     "telephone"
    t.integer  "shop_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.text     "address"
    t.index ["email"], name: "index_employees_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true, using: :btree
    t.index ["shop_id"], name: "index_employees_on_shop_id", using: :btree
    t.index ["uid"], name: "index_employees_on_uid", unique: true, using: :btree
  end

  create_table "models", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_models_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_models_on_reset_password_token", unique: true, using: :btree
  end

  create_table "order_product_add_ons", force: :cascade do |t|
    t.text     "uid"
    t.integer  "shop_product_add_on_id"
    t.integer  "order_product_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["order_product_id"], name: "index_order_product_add_ons_on_order_product_id", using: :btree
    t.index ["shop_product_add_on_id"], name: "index_order_product_add_ons_on_shop_product_add_on_id", using: :btree
    t.index ["uid"], name: "index_order_product_add_ons_on_uid", unique: true, using: :btree
  end

  create_table "order_products", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.decimal  "price"
    t.integer  "quantity"
    t.integer  "order_id"
    t.integer  "shop_product_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["order_id"], name: "index_order_products_on_order_id", using: :btree
    t.index ["shop_product_id"], name: "index_order_products_on_shop_product_id", using: :btree
    t.index ["uid"], name: "index_order_products_on_uid", unique: true, using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.text     "uid"
    t.decimal  "total_price"
    t.integer  "shop_id"
    t.integer  "customer_id"
    t.integer  "status_id"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_orders_on_customer_id", using: :btree
    t.index ["shop_id"], name: "index_orders_on_shop_id", using: :btree
    t.index ["status_id"], name: "index_orders_on_status_id", using: :btree
    t.index ["uid"], name: "index_orders_on_uid", unique: true, using: :btree
  end

  create_table "product_add_ons", force: :cascade do |t|
    t.text     "uid"
    t.decimal  "price"
    t.integer  "product_id"
    t.integer  "add_on_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["add_on_id"], name: "index_product_add_ons_on_add_on_id", using: :btree
    t.index ["product_id"], name: "index_product_add_ons_on_product_id", using: :btree
    t.index ["uid"], name: "index_product_add_ons_on_uid", unique: true, using: :btree
  end

  create_table "product_groups", force: :cascade do |t|
    t.text     "uid"
    t.integer  "business_client_id"
    t.text     "name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_client_id"], name: "index_product_groups_on_business_client_id", using: :btree
    t.index ["uid"], name: "index_product_groups_on_uid", unique: true, using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.decimal  "price"
    t.integer  "quantity"
    t.integer  "business_client_id"
    t.integer  "product_group_id"
    t.jsonb    "tag_criteria"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_client_id"], name: "index_products_on_business_client_id", using: :btree
    t.index ["product_group_id"], name: "index_products_on_product_group_id", using: :btree
    t.index ["uid"], name: "index_products_on_uid", unique: true, using: :btree
  end

  create_table "shop_operation_times", force: :cascade do |t|
    t.text     "uid"
    t.integer  "shop_id"
    t.text     "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shop_id"], name: "index_shop_operation_times_on_shop_id", using: :btree
    t.index ["uid"], name: "index_shop_operation_times_on_uid", unique: true, using: :btree
  end

  create_table "shop_product_add_ons", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.decimal  "price"
    t.integer  "product_add_on_id"
    t.integer  "shop_product_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["product_add_on_id"], name: "index_shop_product_add_ons_on_product_add_on_id", using: :btree
    t.index ["shop_product_id"], name: "index_shop_product_add_ons_on_shop_product_id", using: :btree
    t.index ["uid"], name: "index_shop_product_add_ons_on_uid", unique: true, using: :btree
  end

  create_table "shop_products", force: :cascade do |t|
    t.text     "uid"
    t.decimal  "price"
    t.integer  "quantity"
    t.integer  "shop_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_shop_products_on_product_id", using: :btree
    t.index ["shop_id"], name: "index_shop_products_on_shop_id", using: :btree
    t.index ["uid"], name: "index_shop_products_on_uid", unique: true, using: :btree
  end

  create_table "shops", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.integer  "business_client_id"
    t.text     "address"
    t.text     "telephone"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_client_id"], name: "index_shops_on_business_client_id", using: :btree
    t.index ["uid"], name: "index_shops_on_uid", unique: true, using: :btree
  end

  create_table "statuses", force: :cascade do |t|
    t.text     "uid"
    t.integer  "shop_id"
    t.text     "name"
    t.text     "notification"
    t.integer  "step"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["shop_id"], name: "index_statuses_on_shop_id", using: :btree
    t.index ["uid"], name: "index_statuses_on_uid", unique: true, using: :btree
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "time_interval_products", force: :cascade do |t|
    t.text     "uid"
    t.text     "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "shop_id"
    t.integer  "shop_product_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["shop_id"], name: "index_time_interval_products_on_shop_id", using: :btree
    t.index ["shop_product_id"], name: "index_time_interval_products_on_shop_product_id", using: :btree
    t.index ["uid"], name: "index_time_interval_products_on_uid", unique: true, using: :btree
  end

  add_foreign_key "add_ons", "business_clients"
  add_foreign_key "admins", "business_clients"
  add_foreign_key "customers", "business_clients"
  add_foreign_key "employees", "shops"
  add_foreign_key "order_product_add_ons", "order_products"
  add_foreign_key "order_product_add_ons", "shop_product_add_ons"
  add_foreign_key "order_products", "orders"
  add_foreign_key "order_products", "shop_products"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "shops"
  add_foreign_key "orders", "statuses"
  add_foreign_key "product_add_ons", "add_ons"
  add_foreign_key "product_add_ons", "products"
  add_foreign_key "product_groups", "business_clients"
  add_foreign_key "products", "business_clients"
  add_foreign_key "products", "product_groups"
  add_foreign_key "shop_operation_times", "shops"
  add_foreign_key "shop_product_add_ons", "product_add_ons"
  add_foreign_key "shop_product_add_ons", "shop_products"
  add_foreign_key "shop_products", "products"
  add_foreign_key "shop_products", "shops"
  add_foreign_key "shops", "business_clients"
  add_foreign_key "statuses", "shops"
  add_foreign_key "time_interval_products", "shop_products"
  add_foreign_key "time_interval_products", "shops"
end
