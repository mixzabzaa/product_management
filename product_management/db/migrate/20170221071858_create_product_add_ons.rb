class CreateProductAddOns < ActiveRecord::Migration[5.0]
  def change
    create_table :product_add_ons do |t|
      t.text :uid
      t.decimal :price
      t.references :product, foreign_key: true
      t.references :add_on, foreign_key: true

      t.timestamps
    end
    add_index :product_add_ons, :uid, unique: true
  end
end
