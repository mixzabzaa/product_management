class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.text :uid
      t.decimal :total_price
      t.references :shop, foreign_key: true
      t.references :customer, foreign_key: true
      t.references :status, foreign_key: true
      t.text :description

      t.timestamps
    end
    add_index :orders, :uid, unique: true
  end
end
