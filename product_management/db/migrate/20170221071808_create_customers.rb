class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.text :uid
      t.text :title
      t.text :first_name
      t.text :last_name
      t.text :email
      t.text :telephone
      t.text :address
      t.references :business_client, foreign_key: true

      t.timestamps
    end
    add_index :customers, :uid, unique: true
  end
end
