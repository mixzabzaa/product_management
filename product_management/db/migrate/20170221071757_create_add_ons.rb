class CreateAddOns < ActiveRecord::Migration[5.0]
  def change
    create_table :add_ons do |t|
      t.text :uid
      t.text :name
      t.integer :price
      t.references :business_client, foreign_key: true

      t.timestamps
    end
    add_index :add_ons, :uid, unique: true
  end
end
