class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.text :uid
      t.text :title
      t.text :first_name
      t.text :last_name
      t.text :telephone
      t.references :shop, foreign_key: true

      t.timestamps
    end
    add_index :employees, :uid, unique: true
  end
end
