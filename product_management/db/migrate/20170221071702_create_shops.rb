class CreateShops < ActiveRecord::Migration[5.0]
  def change
    create_table :shops do |t|
      t.text :uid
      t.text :name
      t.references :business_client, foreign_key: true
      t.text :address
      t.text :telephone

      t.timestamps
    end
    add_index :shops, :uid, unique: true
  end
end
