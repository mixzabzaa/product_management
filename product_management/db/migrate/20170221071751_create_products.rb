class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.text :uid
      t.text :name
      t.decimal :price
      t.integer :quantity
      t.references :business_client, foreign_key: true
      t.references :product_group, foreign_key: true
      t.jsonb :tag_criteria

      t.timestamps
    end
    add_index :products, :uid, unique: true
  end
end
