class CreateShopProductAddOns < ActiveRecord::Migration[5.0]
  def change
    create_table :shop_product_add_ons do |t|
      t.text :uid
      t.text :name
      t.decimal :price
      t.references :product_add_on, foreign_key: true
      t.references :shop_product, foreign_key: true

      t.timestamps
    end
    add_index :shop_product_add_ons, :uid, unique: true
  end
end
