class ChangeTagCiteriaToTagCriteria < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :tag_criteria, :tag_criteria
  end
end
