class CreateBusinessClients < ActiveRecord::Migration[5.0]
  def change
    create_table :business_clients do |t|
      t.text :uid
      t.text :name
      t.text :key

      t.timestamps
    end
    add_index :business_clients, :uid, unique: true
    add_index :business_clients, :key, unique: true
  end
end
