class CreateOrderProductAddOns < ActiveRecord::Migration[5.0]
  def change
    create_table :order_product_add_ons do |t|
      t.text :uid
      t.references :shop_product_add_on, foreign_key: true
      t.references :order_product, foreign_key: true

      t.timestamps
    end
    add_index :order_product_add_ons, :uid, unique: true
  end
end
