class CreateShopOperationTimes < ActiveRecord::Migration[5.0]
  def change
    create_table :shop_operation_times do |t|
      t.text :uid
      t.references :shop, foreign_key: true
      t.text :name
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
    add_index :shop_operation_times, :uid, unique: true
  end
end
