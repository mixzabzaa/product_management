class CreateAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :admins do |t|
      t.text :uid
      t.text :first_name
      t.text :last_name
      t.references :business_client, foreign_key: true

      t.timestamps
    end
    add_index :admins, :uid, unique: true
  end
end
