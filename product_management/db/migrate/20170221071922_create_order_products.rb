class CreateOrderProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :order_products do |t|
      t.text :uid
      t.text :name
      t.decimal :price
      t.integer :quantity
      t.references :order, foreign_key: true
      t.references :shop_product, foreign_key: true

      t.timestamps
    end
    add_index :order_products, :uid, unique: true
  end
end
