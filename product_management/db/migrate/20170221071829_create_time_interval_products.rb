class CreateTimeIntervalProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :time_interval_products do |t|
      t.text :uid
      t.text :name
      t.datetime :start_time
      t.datetime :end_time
      t.references :shop, foreign_key: true
      t.references :shop_product, foreign_key: true

      t.timestamps
    end
    add_index :time_interval_products, :uid, unique: true
  end
end
