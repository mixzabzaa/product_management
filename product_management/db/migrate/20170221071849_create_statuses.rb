class CreateStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses do |t|
      t.text :uid
      t.references :shop, foreign_key: true
      t.text :name
      t.text :notification
      t.integer :step

      t.timestamps
    end
    add_index :statuses, :uid, unique: true
  end
end
