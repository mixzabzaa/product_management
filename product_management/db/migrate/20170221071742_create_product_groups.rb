class CreateProductGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :product_groups do |t|
      t.text :uid
      t.references :business_client, foreign_key: true
      t.text :name

      t.timestamps
    end
    add_index :product_groups, :uid, unique: true
  end
end
