class CreateShopProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :shop_products do |t|
      t.text :uid
      t.decimal :price
      t.integer :quantity
      t.references :shop, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
    add_index :shop_products, :uid, unique: true
  end
end
